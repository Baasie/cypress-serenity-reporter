/**
 * Module dependencies.
 */

// running in Node
let serenity = require('@serenity-js/core/lib').serenity;
let Outcome = require('@serenity-js/core/lib/domain').Outcome;
let RecordedScene = require('@serenity-js/core/lib/domain').RecordedScene;
let Result = require('@serenity-js/core/lib/domain').Result;
let SceneFinished = require('@serenity-js/core/lib/domain').SceneFinished;
let SceneStarts = require('@serenity-js/core/lib/domain').SceneStarts;
let Base = require('mocha').reporters.Base;
let reporter = require('@serenity-js/core/lib/reporting');

serenity.configure({
    crew: [
        // reporter.consoleReporter(),
        reporter.serenityBDDReporter(),
    ],
});
/**
 * Expose `SerenityBDD`.
 */
exports = module.exports = SerenityBDD;

function configureDefaults(options) {
    options = options || {};

}

/**
 * Initialize a new `SerenityBDD` reporter.
 *
 * @param {Runner} runner
 * @api public
 */

function SerenityBDD(runner) {
    Base.call(this, runner);
    runner.on('test', function (scenario) {
        serenity.notify(startOf(scenario));
    });

    runner.on('test end', function (scenario) {
        if (isPending(scenario)) {
            serenity.notify(startOf(scenario));
        }
        serenity.notify(endOf(scenario));
    });

}

function isPending(scenario) {
    return finalStateOf(scenario)  === Result.PENDING;
}

function startOf(scenario) {
    return new SceneStarts(new MochaScene(scenario));
}

function endOf(scenario) {
    return new SceneFinished(new Outcome(new MochaScene(scenario), finalStateOf(scenario), scenario.err));
}

function finalStateOf(scenario) {
    if (scenario.pending) {
        return Result.PENDING;
    }

    if (scenario.state === 'passed') {
        return Result.SUCCESS;
    }

    if (timedOut(scenario) || hasErrors(scenario)) {
        return Result.ERROR;
    }

    if (scenario.state === 'failed') {
        return Result.FAILURE;
    }

    return Result.COMPROMISED;
}

function timedOut(scenario) {
    // Mocha sets the `timedOut` flag *after* notifying the reporter of a test failure, hence the regex check.
    return scenario.timedOut || (scenario.err && /^Timeout.*exceeded/.test(scenario.err.message));
}

function hasErrors(scenario) {
    return scenario.err && scenario.err.constructor && !/AssertionError/.test(scenario.err.name);
}

class MochaScene extends RecordedScene {
    constructor(scenario) {
        super(
            nameOf(scenario),
            categoryOf(scenario),
            { path: 'not available for cypress.' },
            [],
            scenario.fullTitle()
        )
    }
}

function nameOf(scenario) {
    return fullNameOf(scenario).substring(categoryOf(scenario).length).trim();
}

function fullNameOf(scenario) {
    return !!scenario.parent
        ? `${ fullNameOf(scenario.parent) } ${ scenario.title }`.trim()
        : scenario.title;
}

function categoryOf(scenario) {
    return !!scenario.parent && scenario.parent.title.trim() !== ''
        ? categoryOf(scenario.parent)
        : scenario.title;
}