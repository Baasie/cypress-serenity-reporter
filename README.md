# cypress-serenity-reporter

An npm mocha reporter for serenity-js brewed and working for cypress.  

This one is based on Jan Molak his reporter:  
https://github.com/jan-molak/serenity-js/tree/master/packages/mocha/src

But could not get the compiled version of that one working with cypress.  
This is just a simple rewrite in javascript made into an npm module for easy use with cypress:


https://docs.cypress.io/guides/guides/reporters.html# describes using third party reporters in mocha.

```
npm install --save-dev cypress-serenity-reporter
```

then in your cypress.json add the following line:  

  "reporter": "cypress-serenity-reporter"
